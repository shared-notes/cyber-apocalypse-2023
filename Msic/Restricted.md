From docker file I found that flag is beind renamed with some random strings with `flag_` prefix in root directory

![Pasted image 20230322214944.png](../assets/Pasted%20image%2020230322214944.png)

![Pasted image 20230322215003.png](../assets/Pasted%20image%2020230322215003.png)

So our flag will look like `flag_t04P0`

![Pasted image 20230322220950.png](../assets/Pasted%20image%2020230322220950.png)

There is restricted commands,so to list available commands use `compgen -ac`

![Pasted image 20230322220856.png](../assets/Pasted%20image%2020230322220856.png)

let's list files using echo and wildcard `echo /*` (echo under / root directory * everything that exist)

![Pasted image 20230322220707.png](../assets/Pasted%20image%2020230322220707.png)

Our flag is inside `/flag_8dpsy`
Now time to read contents of file.

````bash
while read line; do echo $line; done </flag_8dpsy
````

![Pasted image 20230322221523.png](../assets/Pasted%20image%2020230322221523.png)

Flag: `HTB{r35tr1ct10n5_4r3_p0w3r1355}`
