# Cyber Apocalypse 2023

### Web
- [Passman](./Web/Passman.md)
- [Orbital](./Web/Orbital.md)
- [Drobots](./Web/Drobots.md)
- [Disactic Octo Passles](./Web/Didactic%20Octo%20Paddles.md)

### Msic
- [Hijack](./Msic/Hijack.md)
- [Remote computation](./Msic/Remote%20computation.md)
- [Restricted](./Msic/Restricted.md)
